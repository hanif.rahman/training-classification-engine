# Create Logs

import logging
import os
import sys

def create_logs(log_dir, log_file='log.txt'):
    """Creates a log file and a log stream.
    """
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_file = os.path.join(log_dir, log_file)
    log_format = '%(asctime)s %(message)s'
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format=log_format, datefmt='%m/%d %I:%M:%S %p')
    fh = logging.FileHandler(log_file)
    fh.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(fh)
    return logging

#   # Example usage:
#   import logging
#   import os
#   import sys
#   
#   from logs import create_logs
#
#   def main():
#       log_dir = 'logs'
#       logger = create_logs(log_dir)
#       logger.info('Hello, world!')
#
#   if __name__ == '__main__':
#       main()

