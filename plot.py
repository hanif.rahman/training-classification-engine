import matplotlib.pyplot as plt
import numpy as np
import os


def create_plot(plot_training, model_name, criterion, optimizer, log_dir, datetime_object):
    plot_dir = os.path.join(log_dir, 'plots')
    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)

    # Extract data from plot_training
    loss_train = plot_training[0]
    loss_val = plot_training[1]
    acc_train = plot_training[2]
    acc_val = plot_training[3]
    best_acc = plot_training[4]

    # Plot the Loss in Left Figure and Accuracy in Right Figure
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))
    fig.suptitle(f'Model: {model_name}, Criterion: {criterion}, Optimizer: {optimizer}, Best Accuracy: {best_acc:.4f}', fontsize=16)

    # Plot Loss
    ax1.plot(loss_train, label='Train')
    ax1.plot(loss_val, label='Val')
    ax1.set_title('Loss')
    ax1.set_xlabel('Epoch')
    ax1.set_ylabel('Loss')
    ax1.legend()

    # Plot Accuracy
    ax2.plot(acc_train, label='Train')
    ax2.plot(acc_val, label='Val')
    ax2.set_title('Accuracy')
    ax2.set_xlabel('Epoch')
    ax2.set_ylabel('Accuracy')
    ax2.legend()

    # Save the plot
    plt.savefig(os.path.join(plot_dir, f'plot_{datetime_object}.png'))