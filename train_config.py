# Various of Loss Criterion
# -------------------------

import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler

from focal_loss import FocalLoss

# Various of Loss Criterion
# -------------------------
def get_criterion(criterion_name):
    if criterion_name == 'cross_entropy':
        return nn.CrossEntropyLoss()
    elif criterion_name == 'mse':
        return nn.MSELoss()
    elif criterion_name == 'bce':
        return nn.BCELoss()
    elif criterion_name == 'bce_with_logits':
        return nn.BCEWithLogitsLoss()
    elif criterion_name == 'nll':
        return nn.NLLLoss()
    elif criterion_name == 'focal_loss':
        return FocalLoss()
    else:
        raise Exception('Unknown criterion: {}'.format(criterion_name))

# Various of Optimizer
# --------------------
def get_optimizer(optimizer_name, model, lr, momentum, weight_decay):
    if optimizer_name == 'sgd':
        return optim.SGD(model.parameters(), lr=lr, momentum=momentum, weight_decay=weight_decay)
    elif optimizer_name == 'adam':
        return optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
    elif optimizer_name == 'adadelta':
        return optim.Adadelta(model.parameters(), lr=lr, weight_decay=weight_decay)
    elif optimizer_name == 'adagrad':
        return optim.Adagrad(model.parameters(), lr=lr, weight_decay=weight_decay)
    elif optimizer_name == 'rmsprop':
        return optim.RMSprop(model.parameters(), lr=lr, momentum=momentum, weight_decay=weight_decay)
    else:
        raise Exception('Unknown optimizer: {}'.format(optimizer_name))

# Various of Scheduler
# --------------------
def get_scheduler(scheduler_name, optimizer, step_size, gamma):
    if scheduler_name == 'step_lr':
        return lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=gamma)
    elif scheduler_name == 'multi_step_lr':
        return lr_scheduler.MultiStepLR(optimizer, milestones=[30, 80], gamma=0.1)
    elif scheduler_name == 'exponential_lr':
        return lr_scheduler.ExponentialLR(optimizer, gamma=gamma)
    elif scheduler_name == 'cosine_annealing_lr':
        return lr_scheduler.CosineAnnealingLR(optimizer, T_max=10)
    elif scheduler_name == 'reduce_lr_on_plateau':
        return lr_scheduler.ReduceLROnPlateau(optimizer, 'min')
    else:
        raise Exception('Unknown scheduler: {}'.format(scheduler_name))
