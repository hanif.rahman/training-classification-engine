import torch
import time
import copy

def train_model(model, criterion, optimizer, scheduler, device, dataloaders, dataset_sizes, logger, classes, num_epoch=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    # ================================================
    loss_train = []
    loss_val = []
    acc_train = []
    acc_val = []

    for epoch in range(num_epoch):
        text_epoch = 'Epoch {}/{}'.format(epoch, num_epoch - 1)
        text_dash = '-' * 10
        logger.info(text_epoch)
        logger.info(text_dash)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            class_correct = list(0. for i in range(len(classes)))
            class_total = list(0. for i in range(len(classes)))

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

                # Per-class accuracy computation
                c = (preds == labels).squeeze()
                # print(f'c: {c}')
                # print(f'class_correct: {class_correct}')

                if c.dim() == 0:  # Check if c is a scalar value
                    c = c.unsqueeze(0)  # Make c a 1-element tensor
                # print(f'c: {c}')

                for i in range(len(labels)):
                    label = labels[i]
                    # print(f'label: {label}')
                    # print(f'labels[i]: {labels[i]}')
                    # print(f'c[i].item(): {c[i].item()}')
                    class_correct[label] += c[i].item()
                    class_total[label] += 1

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            text_phase_loss = '{} Loss: {:.4f} Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc)
            logger.info(text_phase_loss) 

            # Print per-class accuracies
            if phase == 'val':
                text_devided = "--"*30
                logger.info(text_devided)  
                for i in range(len(classes)):
                    text_class_acc = 'Accuracy of class {}: {:.2f}%'.format(classes[i], 100 * class_correct[i] / class_total[i])
                    logger.info(text_class_acc)        
            
            # ================================================ Prepare for plotting
            if phase == 'train':
                loss_train.append(round(epoch_loss, 4))
                acc_train.append(round(epoch_acc.item(), 4))
            else:
                loss_val.append(round(epoch_loss, 4))
                acc_val.append(round(epoch_acc.item(), 4))

            # deep copy the model
            if phase == 'val':

                if epoch_acc > best_acc:
                    # Save the best model
                    torch.save(model.state_dict(), 'checkpoints/best_model_checkpoints.pt')
                    text_save = '==========> Best Model saved'

                    best_acc = epoch_acc
                    best_model_wts = copy.deepcopy(model.state_dict())
                else:
                    torch.save(model.state_dict(), 'checkpoints/last_model_checkpoints.pt')
                    text_save = '==> Last Model saved'
                logger.info(text_save)

        print()

    time_elapsed = time.time() - since

    text_elapsed = 'Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60)
    text_best = 'Best val Acc: {:4f}'.format(best_acc)
    logger.info(text_elapsed)
    logger.info(text_best)

    # ================================================ Preaper Plot
    plot_variable = [loss_train, loss_val, acc_train, acc_val, round(best_acc.item(), 4)]
    # load best model weights
    model.load_state_dict(best_model_wts)

    # Return model and the plot
    return model, plot_variable