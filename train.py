import argparse
import os
import torch
import datetime
import matplotlib.pyplot as plt

from dataset import get_data_loader
from engine import train_model
from model import get_model
from train_config import get_criterion, get_optimizer, get_scheduler
from logs import create_logs
from plot import create_plot

import sys
sys.dont_write_bytecode = True

datetime_object = datetime.datetime.now()
datetime_object = datetime_object.strftime("%d-%m-%Y_%H-%M-%S")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='data/cats_and_dogs')
    parser.add_argument('--model_name', type=str, default='resnet50')
    parser.add_argument('--batch_size', type=int, default=4)
    parser.add_argument('--num_epoch', type=int, default=25)
    parser.add_argument('--size', type=int, default=224)
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--momentum', type=float, default=0.9)
    parser.add_argument('--weight_decay', type=float, default=0.0001)
    parser.add_argument('--step_size', type=int, default=7)
    parser.add_argument('--gamma', type=float, default=0.1)
    parser.add_argument('--num_classes', type=int, default=4)
    parser.add_argument('--num_workers', type=int, default=4)
    parser.add_argument('--criterion', type=str, default='cross_entropy')
    parser.add_argument('--optimizer', type=str, default='adam')
    parser.add_argument('--scheduler', type=str, default='step_lr')
    parser.add_argument('--device', type=str, default='cuda')
    parser.add_argument('--save_dir', type=str, default='checkpoints')
    parser.add_argument('--log_dir', type=str, default=f'logs')
    args = parser.parse_args()

    # Create logs
    logger = create_logs(args.log_dir, log_file=f'logs_{datetime_object}.txt')

    dataloaders, dataset_sizes, class_names = get_data_loader(args.data_dir, args.batch_size, args.size)
    
    args.num_classes = len(class_names)
    text_number_of_classes = 'Number of classes: {}'.format(args.num_classes)
    text_number_of_train_images = 'Number of train images: {}'.format(dataset_sizes['train'])
    text_number_of_val_images = 'Number of val images: {}'.format(dataset_sizes['val'])
    text_classes_names = 'Classes names: {}'.format(class_names)
    text_model_name = 'Model name: {}'.format(args.model_name)
    text_crierion_name = 'Crierion name: {}'.format(args.criterion)
    text_optimizer_name = 'Optimizer name: {}'.format(args.optimizer)
    logger.info(text_number_of_classes)
    logger.info(text_number_of_train_images)
    logger.info(text_number_of_val_images)
    logger.info(text_classes_names)
    logger.info(text_model_name)
    logger.info(text_crierion_name)
    logger.info(text_optimizer_name)
    
    model = get_model(args.model_name, args.num_classes, args.device)
    
    criterion = get_criterion(args.criterion)
    optimizer = get_optimizer(args.optimizer, model, args.lr, args.momentum, args.weight_decay)
    scheduler = get_scheduler(args.scheduler, optimizer, args.step_size, args.gamma)
    
    best_model, plot_training = train_model(model, criterion, optimizer, scheduler, args.device, dataloaders, dataset_sizes, logger, class_names, args.num_epoch)

    # Add Model Name, Criterion Name, Optimizer Name to plot title
    create_plot(plot_training, model_name=args.model_name, criterion=args.criterion, optimizer=args.optimizer, log_dir=args.log_dir, datetime_object=datetime_object)
    print('Done Plotting')
    print("=======================================================")

    # Save the best model
    if not os.path.exists(args.save_dir):
        os.makedirs(args.save_dir)
    torch.save(best_model.state_dict(), os.path.join(args.save_dir, f'best_model_{args.num_classes}_{args.model_name}_{args.size}_{datetime_object}.pth'))



if __name__ == '__main__':
    main()


