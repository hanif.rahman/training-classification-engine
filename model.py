import torch
import torch.nn as nn
import torchvision.models as models

class Model(torch.nn.Module):
    def __init__(self, input_size, hidden_size, output_size, transfer_learning=False):
        super(Model, self).__init__()
        self.linear1 = torch.nn.Linear(input_size, hidden_size)
        self.linear2 = torch.nn.Linear(hidden_size, output_size)
        self.relu = torch.nn.ReLU()

    def forward(self, x):
        out = self.linear1(x)
        out = self.relu(out)
        out = self.linear2(out)
        return out

class CNNModel(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(CNNModel, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=1)
        self.relu1 = nn.ReLU()
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.relu2 = nn.ReLU()
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.fc1 = nn.Linear(64 * 56 * 56, 128)
        self.relu3 = nn.ReLU()
        self.fc2 = nn.Linear(128, num_classes)

    def forward(self, x):
        out = self.conv1(x)
        out = self.relu1(out)
        out = self.conv2(out)
        out = self.relu2(out)
        out = self.maxpool(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc1(out)
        out = self.relu3(out)
        out = self.fc2(out)
        return out

class Resnet50(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(Resnet50, self).__init__()
        self.transfer_learning = transfer_learning
        self.resnet = models.resnet50(pretrained=True)
        if not self.transfer_learning:
            for param in self.resnet.parameters():
                param.requires_grad = False
        self.resnet.fc = nn.Linear(2048, num_classes)

    def forward(self, x):
        return self.resnet(x)

class InceptionV3(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(InceptionV3, self).__init__()
        self.inception = models.inception_v3(pretrained=True)
        self.inception.fc = nn.Linear(2048, num_classes)

    def forward(self, x):
        return self.inception(x)

class VGG16(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(VGG16, self).__init__()
        self.vgg = models.vgg16(pretrained=True)
        self.vgg.classifier[6] = nn.Linear(4096, num_classes)

    def forward(self, x):
        return self.vgg(x)

class AlexNet(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(AlexNet, self).__init__()
        self.alex = models.alexnet(pretrained=True)
        self.alex.classifier[6] = nn.Linear(4096, num_classes)

    def forward(self, x):
        return self.alex(x)

class DenseNet121(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(DenseNet121, self).__init__()
        self.densenet = models.densenet121(pretrained=True)
        self.densenet.classifier = nn.Linear(1024, num_classes)

    def forward(self, x):
        return self.densenet(x)

class SqueezeNet(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(SqueezeNet, self).__init__()
        self.squeezenet = models.squeezenet1_1(pretrained=True)
        self.squeezenet.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1, 1), stride=(1, 1))
        self.squeezenet.num_classes = num_classes

    def forward(self, x):
        return self.squeezenet(x)

class MobileNetV2(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(MobileNetV2, self).__init__()
        self.mobilenet = models.mobilenet_v2(pretrained=True)
        self.mobilenet.classifier[1] = nn.Linear(1280, num_classes)

    def forward(self, x):
        return self.mobilenet(x)

class ShuffleNetV2(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(ShuffleNetV2, self).__init__()
        self.shufflenet = models.shufflenet_v2_x1_0(pretrained=True)
        self.shufflenet.fc = nn.Linear(1024, num_classes)

    def forward(self, x):
        return self.shufflenet(x)

class VGG19(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(VGG19, self).__init__()
        self.vgg = models.vgg19(pretrained=True)
        self.vgg.classifier[6] = nn.Linear(4096, num_classes)

    def forward(self, x):
        return self.vgg(x)

class EfficientNetV2L(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(EfficientNetV2L, self).__init__()
        self.efficientnet = models.efficientnet_v2_l(pretrained=True)
        self.efficientnet.classifier = nn.Linear(1280, num_classes)

    def forward(self, x):
        return self.efficientnet(x)

class ConvNext_Large(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(ConvNext_Large, self).__init__()
        self.convnext = models.convnext(pretrained=True)
        self.convnext.fc = nn.Linear(2048, num_classes)

    def forward(self, x):
        return self.convnext(x)

class Resnet152(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(Resnet152, self).__init__()
        self.resnet = models.resnet152(pretrained=True)
        self.resnet.fc = nn.Linear(2048, num_classes)

    def forward(self, x):
        return self.resnet(x)
    
class Resnet101(nn.Module):
    def __init__(self, num_classes, transfer_learning=False):
        super(Resnet101, self).__init__()
        self.resnet = models.resnet101(pretrained=True)
        if transfer_learning:
            for param in self.resnet.parameters():
                param.requires_grad = False
        self.resnet.fc = nn.Linear(2048, num_classes)

    def forward(self, x):
        return self.resnet(x)

def get_model(model_name, num_classes, device, transfer_learning=False):
    if model_name == 'resnet50':
        model = Resnet50(num_classes, transfer_learning)
    elif model_name == 'resnet101':
        model = Resnet101(num_classes, transfer_learning)
    elif model_name == 'inceptionv3':
        model = InceptionV3(num_classes, transfer_learning)
    elif model_name == 'vgg16': 
        model = VGG16(num_classes, transfer_learning)
    elif model_name == 'alexnet':
        model = AlexNet(num_classes, transfer_learning)
    elif model_name == 'densenet121':
        model = DenseNet121(num_classes, transfer_learning)
    elif model_name == 'squeezenet':
        model = SqueezeNet(num_classes, transfer_learning)
    elif model_name == 'mobilenetv2':
        model = MobileNetV2(num_classes, transfer_learning)
    elif model_name == 'shufflenetv2':
        model = ShuffleNetV2(num_classes, transfer_learning)
    elif model_name == 'vgg19':
        model = VGG19(num_classes, transfer_learning)
    elif model_name == 'efficientnetv2l':
        model = EfficientNetV2L(num_classes, transfer_learning)
    elif model_name == 'convnext_large':
        model = ConvNext_Large(num_classes, transfer_learning)
    elif model_name == 'resnet152':
        model = Resnet152(num_classes, transfer_learning)
    elif model_name == 'cnn':
        model = CNNModel(num_classes, transfer_learning)
    else:
        raise ValueError('Invalid model name')

    model = model.to(device)
    return model